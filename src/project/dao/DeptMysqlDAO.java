package project.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

import project.dto.DeptDTO;

public class DeptMysqlDAO implements DeptDao {

	@Autowired
	SqlSession ss;

	@Override
	public List<DeptDTO> selectAll() {
		
		return ss.selectList("project.dept.selectAll");
	}

	@Override
	public DeptDTO selectOne(int no) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertOne(DeptDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateOne(DeptDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteOne(int no) {
		// TODO Auto-generated method stub
		
	}

}
