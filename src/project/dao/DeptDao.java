package project.dao;

import java.util.List;

import project.dto.DeptDTO;

public interface DeptDao {

	public List<DeptDTO>selectAll();
	public DeptDTO selectOne(int no);
	public void insertOne(DeptDTO dto);
	public void updateOne(DeptDTO dto);
	public void deleteOne(int no);

}