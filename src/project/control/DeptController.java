package project.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import project.dao.DeptDao;

@Controller
public class DeptController {
	
	@Autowired
	DeptDao ddao;
	
	@RequestMapping("/list")
	public ModelAndView DeptList(){
		return new ModelAndView("list","list",ddao.selectAll());
	}
}