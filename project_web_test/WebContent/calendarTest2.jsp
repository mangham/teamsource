<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
	integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r"
	crossorigin="anonymous">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
	integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
	crossorigin="anonymous"></script>
<script type="text/javascript">
	$(function() {
		$('.sched').on("click", function() {
			$('#myModal').on('shown.bs.modal', function() {
				$('#myInput').focus()
			});
		});
	})
</script>
</head>
<body>
	<%
		Calendar cal = Calendar.getInstance();
		int nowYear = cal.get(Calendar.YEAR);
		int nowMonth = cal.get(Calendar.MONTH) + 1;
		int nowDay = cal.get(Calendar.DAY_OF_MONTH);

		String param_month = request.getParameter("param_month");

		if (param_month != null) {
			cal.set(Calendar.MONTH, Integer.parseInt(param_month) - 1);
		}

		nowMonth = cal.get(Calendar.MONTH) + 1;

		String param_year = request.getParameter("param_year");

		if (param_year != null) {
			cal.set(Calendar.YEAR, Integer.parseInt(param_year));
		}

		nowYear = cal.get(Calendar.YEAR);

		StringBuilder paramDay = new StringBuilder();
		paramDay.append(nowYear).append(nowMonth);
	%>
	<div class="container">
		<div id="diaryTitle">
			<a
				href="calendarTest2.jsp?param_month=<%=nowMonth - 1 == 0 ? 12 : nowMonth - 1%>&param_year=<%=nowMonth - 1 == 0 ? nowYear - 1 : nowYear%>">
				<button type="button" class="btn" id="btn1">
					<span class="glyphicon glyphicon-menu-left"> </span>
				</button>
			</a> <span> <%=nowYear%> <%=nowMonth%></span> <a
				href="calendarTest2.jsp?param_month=<%=nowMonth + 1 == 13 ? 1 : nowMonth + 1%>&param_year=<%=nowMonth + 1 == 13 ? nowYear + 1 : nowYear%>">
				<button type="button" class="btn" id="btn2">
					<span class="glyphicon glyphicon-menu-right"> </span>
				</button>
			</a>
		</div>
		<div id="diaryContent">
			<table class="table">
				<thead>
					<tr>
						<th>일</th>
						<th>월</th>
						<th>화</th>
						<th>수</th>
						<th>목</th>
						<th>금</th>
						<th>토</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<%
							for (int tempDay = 1;; tempDay++) {
								cal.set(Calendar.DAY_OF_MONTH, tempDay);
								if (tempDay != cal.get(Calendar.DAY_OF_MONTH)) {
									break;
								} // if end
								switch (tempDay) {
								case 1:
									int week = cal.get(Calendar.DAY_OF_WEEK);
									for (int black = 1; black < week; black++) {
						%>
						<td class="blank">&nbsp;</td>
						<%
							} // for end
								} // switch end
						%>
						<td class="sched" type="button" data-target="#myModal"
							data-toggle="modal"><%=tempDay%></td>
						<%
							//설정된 일자가 토요일이면 행을 변경한다.
								switch (cal.get(Calendar.DAY_OF_WEEK)) {
								case Calendar.SATURDAY:
						%>
					</tr>
					<tr>
						<%
							}
							} //end for​
						%>
					</tr>
				</tbody>
			</table>
		</div>

		<button type="button" class="btn btn-default" id="btnmodal"
			data-target="#myModal" data-toggle="modal">ddfafdasfda</button>

		​

		<div id="footer">copy right &copy; sist class 4 (한보경 o(^.^)o )</div>
	</div>

	<form action="scheduleOk">
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">일정 입력</h4>
					</div>
					<div class="modal-body"><%=nowYear %>-<%=nowMonth %></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save
							changes</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
</html>
